#!/bin/bash

#####
# Show usage
#####
function usage()
{
    echo
    echo "Comands:"
    echo "  install                              Install all the required components to install Ansible"
    echo
    echo "Options:"
    echo "  -h, --help                           Show usage"
    echo "  --proxy-protocol                     HTTP(S) proxy protocol (http or https)"
    echo "  --proxy-host                         HTTP(S) proxy host (host[:port])"
    echo "  --proxy-username                     HTTP(S) proxy username"
    echo "  --proxy-password                     HTTP(S) proxy password"

    if [ "$COMMAND" = "install" ]; then
      echo
      echo "  Defaults:"
      echo "    info: use the pip versionning pattern (--ansible-version ==2.8, --ansible-version >=2.8,<2.9)"
      echo "    --pip-version                      PIP version"
      echo "    --wheel-version                    Wheel version"
      echo "    --setuptools-version               Setuptools version"
      echo "    --ansible-version                  Ansible version"
    fi
    
    echo
    echo "Usage:"
    echo "  ./installer.sh [command] [options]"
    echo "  ./installer.sh --help"
    echo "  ./installer.sh [command] --help" 

    if [ "$COMMAND" = "install" ]; then
      echo "  ./installer.sh install" 
      echo "  ./installer.sh install --pip-version ==19.2.3" 
    fi

    echo
    exit 0
}

###
# Setup the http_proxy and https_proxy environment variables
# Must provide at least the proxy host
###
function set_proxy() {
  if [ ! -z "$PROXY_HOST" ] ; then
    # build proxy url <[protocol://][user:password@]host[:port]>
    proxy=""
    if [[ ! -z "$PROXY_USERNAME" ]] ; then 
      proxy="$PROXY_USERNAME" 
    fi
    if [[ ! -z "$PROXY_PASSWORD" ]] ; then 
      proxy="$proxy:$PROXY_PASSWORD@" 
    fi
    if [[ ! -z "$proxy" ]] ; then 
      proxy="$proxy@" 
    fi
    proxy="$proxy$PROXY_HOST"

    # Set global variables executable using those variables 
    export http_proxy="$PROXY_PROTOCOL://$proxy"
    export https_proxy="$PROXY_PROTOCOL://$proxy"

    echo "Http proxy set to $PROXY_PROTOCOL://$proxy"
  fi
} 

function install() {

  # Show help and exit  
  [[ "$HELP" -eq 1 ]] && usage

  # Python is required
  # This script do not handle python installation since it is installed per default in major linux distribution
  if [ ! -x "$(command -v python)" ]; then
    echo "You must install python first"
    exit 1
  fi

  set_proxy

  # If pip is not already installed 
  if [ ! -x "$(command -v pip)" ]; then
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

    pip_cmd="sudo python get-pip.py"
    [[ ! -z "$PIP_VERSION" ]] && pip_cmd="$pip_cmd pip$PIP_VERSION"
    [[ ! -z "$WHEEL_VERSION" ]] && pip_cmd="$pip_cmd wheel$WHEEL_VERSION"
    [[ ! -z "$SETUPTOOLS_VERSION" ]] && pip_cmd="$pip_cmd setuptools$SETUPTOOLS_VERSION"
    eval ${pip_cmd}
  else 
    echo "pip already installed."
  fi

  # If ansible is not already installed
  if [ ! -x "$(command -v ansible)" ]; then
    ansible_cmd="sudo pip install ansible"
    [[ ! -z "$ANSIBLE_VERSION" ]] && ansible_cmd="$ansible_cmd$ANSIBLE_VERSION"
    eval ${ansible_cmd}
  else 
    echo "ansible already installed."
  fi
}

# Handle scritps arguments
ARGS_COUNT="$#"
COMMAND="install"
HELP=0
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --pip-version)
    PIP_VERSION="$2"
    shift
    shift
    ;;
    --wheel-version)
    WHEEL_VERSION="$2"
    shift
    shift
    ;;
    --setuptools-version)
    SETUPTOOLS_VERSION="$2"
    shift
    shift
    ;;
    --ansible-version)
    ANSIBLE_VERSION="$2"
    shift
    shift
    ;;
    --proxy-protocol)
    PROXY_PROTOCOL="$2"
    shift
    shift
    ;;
    --proxy-host)
    PROXY_HOST="$2"
    shift
    shift
    ;;
    --proxy-username)
    PROXY_USERNAME="$2"
    shift
    shift
    ;;
    --proxy-password)
    PROXY_PASSWORD="$2"
    shift
    shift
    ;;
    install)
    COMMAND="install"
    shift
    ;;
    -h|--help|*)
    POSITIONAL+=("$1")
    HELP=1
    shift
    ;;
esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters
set -e 

PIP_VERSION=${PIP_VERSION:=""}
WHEEL_VERSION=${WHEEL_VERSION:=""}
SETUPTOOLS_VERSION=${SETUPTOOLS_VERSION:=""}
ANSIBLE_VERSION=${ANSIBLE_VERSION:=""}

PROXY_PROTOCOL=${PROXY_PROTOCOL:="http"}
PROXY_HOST=${PROXY_HOST:=""}
PROXY_USERNAME=${PROXY_USERNAME:=""}
PROXY_PASSWORD=${PROXY_PASSWORD:=""}

#  if no args show usage
if [ "$ARGS_COUNT" -eq 0 ]; then
  usage
fi

# run commands 
case $COMMAND in
  # install function
  "install") install;;
  # show usage if now commands
  *) usage;;
esac