# Ansible installer

## Description

This script install pip and ansible.

## Usage

```text
Comands:
  install                              Install all the required components to install Ansible

Options:
  -h, --help                           Show usage
  --proxy-protocol                     HTTP(S) proxy protocol (http or https)
  --proxy-host                         HTTP(S) proxy host (host[:port])
  --proxy-username                     HTTP(S) proxy username
  --proxy-password                     HTTP(S) proxy password

  Defaults:
    info: use the pip versionning pattern (--ansible-version ==2.8, --ansible-version >=2.8,<2.9)
    --pip-version                      PIP version
    --wheel-version                    Wheel version
    --setuptools-version               Setuptools version
    --ansible-version                  Ansible version

Usage:
  ./installer.sh [command] [options]
  ./installer.sh --help
  ./installer.sh [command] --help
  ./installer.sh install
  ./installer.sh install --pip-version ==19.2.3
```

## What's missing

* Test install behind proxy
